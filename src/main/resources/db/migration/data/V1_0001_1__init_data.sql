
INSERT INTO ippo.lecturer_type (id, type, study_load, is_part_time, is_external)
VALUES ('474be1bf-b35d-424e-b841-97550968775e', 'Профессор', 720, false, false);
INSERT INTO ippo.lecturer_type (id, type, study_load, is_part_time, is_external)
VALUES ('474be1bf-b35d-424e-b841-97551968775e', 'Доцент', 810, false, false);
INSERT INTO ippo.lecturer_type (id, type, study_load, is_part_time, is_external)
VALUES ('474be1bf-b35d-424e-b841-97552968775e', 'Старший преподаватель', 880, false, false);
INSERT INTO ippo.lecturer_type (id, type, study_load, is_part_time, is_external)
VALUES ('474be1bf-b35d-424e-b841-97553968775e', 'Ассистент', 880, false, false);
INSERT INTO ippo.lecturer_type (id, type, study_load, is_part_time, is_external)
VALUES ('474be1bf-b35d-424e-b841-97554968775e', 'Заведующий кафедрой', 640, false, false);
INSERT INTO ippo.lecturer_type (id, type, study_load, is_part_time, is_external)
VALUES ('474be1bf-b35d-424e-b841-97555968775e', 'Ассистент', 880, true, false);
INSERT INTO ippo.lecturer_type (id, type, study_load, is_part_time, is_external)
VALUES ('474be1bf-b35d-424e-b841-97556968775e', 'Доцент', 810, true, false);
INSERT INTO ippo.lecturer_type (id, type, study_load, is_part_time, is_external)
VALUES ('474be1bf-b35d-424e-b841-97557968775e', 'Профессор', 720, true, false);

create table ippo.lecturer_degree
(
    degree varchar not null,
    constraint pk_lecturer_degree primary key (degree)
);

create table ippo.lecturer_academic_rank
(
    academic_rank varchar not null,
    constraint pk_lecturer_academic_rank primary key (academic_rank)
);

insert into ippo.lecturer_degree (degree)
values ('Доктор наук');
insert into ippo.lecturer_degree (degree)
values ('Кандидат наук');

insert into ippo.lecturer_academic_rank (academic_rank)
values ('Доцент');
insert into ippo.lecturer_academic_rank (academic_rank)
values ('Профессор');

insert into ippo.user (id, username, password, roles, department) values ('95e0176e-f6a6-4346-ae7d-9033bfaeef1b', 'admin', '$2y$10$/EyYgJI2InYO2lKWmEekUOORubJPPL751vLQMseqHV9y0O0zHlFuS', '{ADMIN}', 7);
insert into ippo.user (id, username, password, roles, department) values ('95e0176e-f6a6-4346-ae7d-9033bfaeef2b', 'head', '$2y$10$/EyYgJI2InYO2lKWmEekUOORubJPPL751vLQMseqHV9y0O0zHlFuS', '{DEPARTMENT_HEAD}', 7);

