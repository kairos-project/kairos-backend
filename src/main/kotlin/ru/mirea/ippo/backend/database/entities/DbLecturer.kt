package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.Lecturer
import ru.mirea.ippo.backend.models.LecturerAcademicRank
import ru.mirea.ippo.backend.models.LecturerDegree
import ru.mirea.ippo.backend.models.LecturerTemplate
import java.math.BigDecimal
import java.time.Instant
import java.util.*


data class DbLecturer (
    @Id
    val id: UUID,
    val lecturerTypeId: UUID,
    val lecturerType: DbLecturerType?,
    val academicRank: String?,
    val degree: String?,
    val name: String,
    val middleName: String,
    val lastName: String,
    val lecturerRate: BigDecimal,
    val lecturerLoadForRate: BigDecimal,
    val lecturerMaxLoadForRate: BigDecimal,
    val employmentStartDate: Instant,
    val employmentFinishDate: Instant,
    val monthAmount: BigDecimal,
    val department: Int
) {
    fun toModel(): Lecturer = Lecturer(
        id,
        lecturerType?.toModel(),
        LecturerAcademicRank(academicRank),
        LecturerDegree(degree),
        name,
        middleName,
        lastName,
        lecturerRate,
        lecturerLoadForRate,
        lecturerMaxLoadForRate,
        employmentStartDate,
        employmentFinishDate,
        monthAmount
    )
    companion object{
        fun fromTemplate(lecturer: LecturerTemplate, lecturerLoadForRate: BigDecimal, lecturerMaxLoadForRate: BigDecimal, department: Int) = DbLecturer(
            lecturer.id ?: UUID.randomUUID(),
            lecturer.lecturerTypeId,
            null,
            lecturer.academicRank?.academicRank,
            lecturer.degree?.degree,
            lecturer.name,
            lecturer.middleName,
            lecturer.lastName,
            lecturer.lecturerRate,
            lecturerLoadForRate,
            lecturerMaxLoadForRate,
            lecturer.employmentStartDate,
            lecturer.employmentFinishDate,
            lecturer.monthAmount,
            department
        )
    }
}