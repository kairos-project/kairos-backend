package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.CurriculumShort
import java.util.*

data class DbCurriculumShort(
    @Id
    val id: UUID,
    val fieldOfStudy: String,
    val educationalProfile: String,
    val startYear: Int
) {
    fun toModel(): CurriculumShort = CurriculumShort(
        id,
        fieldOfStudy,
        educationalProfile,
        startYear
    )
    companion object {
        fun fromTemplate(curriculumShortTemplate: CurriculumShort): DbCurriculumShort = DbCurriculumShort(
            curriculumShortTemplate.id ?: UUID.randomUUID(),
            curriculumShortTemplate.fieldOfStudy,
            curriculumShortTemplate.profile,
            curriculumShortTemplate.startYear
        )
    }

}