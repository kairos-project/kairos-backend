package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.LoadUnit
import java.math.BigDecimal
import java.util.*


data class DbLoadUnit (
    @Id
    val id: UUID,
    val subject: String,
    val course: Int,
    val semester: Int,
    val group: DbGroup?,
    val stream: DbStream?,
    val hours: BigDecimal,
    val hoursType: String,
    val curriculumId: UUID,
    val department: Int,
    val distributedParts: List<DbLoadUnitDistributedPart>
) {
    fun toModel(): LoadUnit = LoadUnit(
        id,
        subject,
        course,
        semester,
        group?.toModel(),
        stream?.toModel(),
        hours,
        hoursType,
        curriculumId,
        department,
        distributedParts.map { it.toModel() }
    )
}