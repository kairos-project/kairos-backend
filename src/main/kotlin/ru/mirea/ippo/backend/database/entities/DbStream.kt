package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.Stream
import java.util.*


data class DbStream (
    @Id
    val id: UUID,
    val type: String,
    val subject: String,
    val course: Int,
    val groups: List<DbGroup>,
    val curriculumId: UUID
) {
    fun toModel() : Stream = Stream(
        id,
        type,
        subject,
        course,
        groups.map { it.toModel() },
        curriculumId
    )
    companion object {
        fun fromTemplate(streamTemplate: Stream): DbStream = DbStream(
            streamTemplate.id ?: UUID.randomUUID(),
            streamTemplate.type,
            streamTemplate.subject,
            streamTemplate.course,
            streamTemplate.groups?.map {  DbGroup.fromTemplate(it) } ?: emptyList(),
            streamTemplate.curriculumId
        )
    }
}