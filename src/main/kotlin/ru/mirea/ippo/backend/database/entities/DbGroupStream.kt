package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import java.util.*


data class DbGroupStream (
    @Id
    val streamId: UUID,
    val groupCode: String
) {
    companion object{
        fun fromGroup(streamId: UUID, groupCode: String) : DbGroupStream = DbGroupStream(
            streamId,
            groupCode
        )
    }
}