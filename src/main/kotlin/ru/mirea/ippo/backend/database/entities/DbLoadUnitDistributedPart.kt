package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.LoadUnitDistributedPart
import java.math.BigDecimal
import java.util.*


data class DbLoadUnitDistributedPart (
    @Id
    val id: UUID,
    val loadUnitId: UUID,
    val lecturerId: UUID,
    val loadUnit: DbLoadUnit?,
    val lecturer: DbLecturer?,
    val loadPart: BigDecimal
) {
    fun toModel(): LoadUnitDistributedPart = LoadUnitDistributedPart(
        id,
        loadUnitId,
        lecturer?.toModel(),
        loadPart
    )
    companion object {
        fun fromTemplate(id: UUID, partId: UUID?, lecturerId: UUID, loadPart: BigDecimal): DbLoadUnitDistributedPart = DbLoadUnitDistributedPart(
            partId ?: UUID.randomUUID(),
            id,
            lecturerId,
            null,
            null,
            loadPart
        )
    }
}