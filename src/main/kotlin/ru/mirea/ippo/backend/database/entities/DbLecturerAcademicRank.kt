package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.LecturerAcademicRank


data class DbLecturerAcademicRank(
    @Id
    val academicRank: String
) {
    fun toModel(): LecturerAcademicRank = LecturerAcademicRank(academicRank)
}