package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.Group
import java.util.*


data class DbGroup (
    @Id
    val code: String,
    val studentsNumber: Int,
    val course: Int,
    val curriculum: DbCurriculumShort?,
    val curriculumId: UUID
) {
    fun toModel() : Group = Group(
        code,
        studentsNumber,
        course,
        curriculum?.toModel(),
        curriculumId
    )
    companion object {
        fun fromTemplate(groupTemplate: Group): DbGroup = DbGroup(
            groupTemplate.code,
            groupTemplate.studentsNumber,
            groupTemplate.course,
            null,
            groupTemplate.curriculumId
        )
    }
}