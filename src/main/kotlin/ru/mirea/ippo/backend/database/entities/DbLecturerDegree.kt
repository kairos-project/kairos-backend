package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.LecturerDegree


data class DbLecturerDegree(
    @Id
    val degree: String
) {
    fun toModel(): LecturerDegree = LecturerDegree(degree)
}