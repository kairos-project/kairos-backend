package ru.mirea.ippo.backend.database.entities

import org.springframework.data.annotation.Id
import ru.mirea.ippo.backend.models.CustomUser
import java.util.*


data class DbUser(
    @Id
    val id: UUID,
    val username: String,
    val password: String,
    val roles: Array<String>,
    val department: Int
) {
    fun toModel(): CustomUser = CustomUser(
        id, username, password, roles
    )
}