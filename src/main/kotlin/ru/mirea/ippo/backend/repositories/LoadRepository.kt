package ru.mirea.ippo.backend.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import ru.mirea.ippo.backend.database.entities.DbLoadUnit
import java.util.*

interface LoadRepository : MongoRepository<DbLoadUnit, UUID> {
    fun findByDepartment(depId: Int): List<DbLoadUnit>
    fun findAllByIdIn(ids: List<UUID>): List<DbLoadUnit>
}