package ru.mirea.ippo.backend.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import ru.mirea.ippo.backend.database.entities.DbUser
import java.util.*

interface UserRepository : MongoRepository<DbUser, UUID> {
    fun findByUsername(username: String): DbUser?
}
