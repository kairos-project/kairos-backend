package ru.mirea.ippo.backend.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import ru.mirea.ippo.backend.database.entities.DbCurriculumShort
import java.util.*

interface CurriculumShortRepository : MongoRepository<DbCurriculumShort, UUID> {
    fun existsByFieldOfStudyAndEducationalProfileAndStartYear(field: String, profile: String, startYear: Int): Boolean
}