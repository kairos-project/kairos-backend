package ru.mirea.ippo.backend.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import ru.mirea.ippo.backend.database.entities.DbGroupStream
import java.util.*

interface GroupStreamRepository : MongoRepository<DbGroupStream, UUID> {
    fun deleteByStreamIdAndGroupCode(streamId: UUID, groupCode: String)
}