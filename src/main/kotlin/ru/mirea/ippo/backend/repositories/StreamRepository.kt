package ru.mirea.ippo.backend.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import ru.mirea.ippo.backend.database.entities.DbStream
import java.util.*

interface StreamRepository : MongoRepository<DbStream, UUID> {
    fun findAllByCurriculumIdAndSubjectAndCourse(curriculumId: UUID, subject: String, course: Int): List<DbStream>
}