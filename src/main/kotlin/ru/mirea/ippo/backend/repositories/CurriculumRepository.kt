package ru.mirea.ippo.backend.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import ru.mirea.ippo.backend.database.entities.DbCurriculumUnit
import java.util.*

interface CurriculumRepository : MongoRepository<DbCurriculumUnit, UUID> {
    fun findAllByCurriculumId(curriculumId: UUID): List<DbCurriculumUnit>
}