package ru.mirea.ippo.backend.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import ru.mirea.ippo.backend.database.entities.DbLecturer
import java.util.*

interface LecturerRepository : MongoRepository<DbLecturer, UUID> {
    fun findAllByDepartment(department: Int): List<DbLecturer>
}
