package ru.mirea.ippo.backend.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import ru.mirea.ippo.backend.database.entities.DbLecturerDegree
import ru.mirea.ippo.backend.database.entities.DbLecturerType
import java.util.*

interface LecturerDegreeRepository : MongoRepository<DbLecturerDegree, String>

